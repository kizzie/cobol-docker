#from kizzie/katbuntu
from ubuntu:14.04
maintainer Kat McIvor

#update
run apt-get update

#pre-requisites
run apt-get install -y open-cobol
run apt-get install -y gcc

#copy in the source file
copy helloworld.cbl /helloworld.cbl

#compiles the code
run cobc -x -free -o helloworld helloworld.cbl

#run the compiled program
cmd ["/helloworld"]

